/*
 * @Author: yuanaohua
 * @Date: 2021-03-22 19:16:07
 * @FilePath: /demo-vue3/demo-vue3/src/main.js
 */
import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

createApp(App).use(store).use(router).use(Antd).mount('#app')
