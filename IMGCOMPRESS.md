<!--
** 前端图片处理库
** 功能包含：图片压缩、裁切、旋转、模糊等
-->

# 图像处理库对比
名称|Image Cropper|blurify|CompressImageUtiles|alpha应用
--|:--:|:--:|:--:|:--|
主要功能|预览、裁剪、旋转JavaScript图像裁剪器|图片模糊化|压缩|图片的第四通道，用于控制透明度
语法类型|jquery插件、canvas|javascript、css.filter属性、canvas|javascript工具类|
兼容性  |谷歌/火狐/safari/IE9+|主流浏览器|谷歌/火狐/safari/IE9+|
输出格式|blob/base64/canvas|修改DOM style|blob/base64/canvas
实现原理|1、两层图片覆盖 一层遮罩 超出隐藏：实现预览区域 2、获取offsetX offsetY，width、height、使用canvas.drawImage（）,实现选中区域的图形绘制| css:filter blur和canvas| 通过设置canvas宽高 及 canvas.toBlob(callback, mimeType, quality);|应用1:background:rgba(255,0,0,0.1);<br/> 应用2:canvas中getContext函数第二个参数，
示例地址|https://www.jq22.com/jquery-info1392|git@github.com:zhangfeng001/blurify-demo.git|git@github.com:zhangfeng001/compressUtls-demo.git
参考地址|https://www.jq22.com/jquery-info1392|git@github.com:zhangfeng001/blurify-demo.git|https://github.com/fengyuanchen/compressorjs

