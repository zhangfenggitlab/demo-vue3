/*
 * @Author: your name
 * @Date: 2021-03-16 11:11:23
 * @LastEditTime: 2021-03-23 16:51:42
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /demo-vue3/demo-vue3/src/store/index.js
 */
import { createStore } from 'vuex'
import vue2 from './modules2/index.js'
import vue3 from './modules3/index.js'
let store = createStore({
  state: {
   name:123
  },
  mutations: {
    ceshi(state){
      state.name=123
    }
  },
  modules: {
    vue2, vue3
  }
})

window.stores = store
window.stores.state.name = {num : 111}
// console.log(window.stores.state.name,"window")
// console.log(store.state.name,"store")
export default store