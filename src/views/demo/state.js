/*
 * @Author: yuanaohua
 * @Date: 2021-03-21 13:19:56
 * @FilePath: /demo-vue3/demo-vue3/src/views/demo/state.js
 */
import { reactive } from 'vue'
let state = reactive({
    student:[
        {id:1,name:'zs',age:10},
        {id:2,name:'ls',age:20},
        {id:3,name:'ww',age:30},
    ]
})
function add(){
    state.student.push({
        id:4,name:'zs22',age:112
    })
}
function jian(){
    state.student.pop()
}
function change(){
    window.states = state
    window.states.student[0].name = {num: 111}
    // input中的数据被改变时会变成 [object Object]
    // console.log(state.student,'state.student')
    // console.log(window.states,'window.states')
}

export default {state,add,jian,change};
